<?php
	if (get_field('brand') == 'Shaw Floors') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/shaw_logo.png" alt="Shaw Floors" class="product-logo" />
<?php } elseif (get_field('brand') == 'Anderson Tuftex') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/anderson_tuftex_logo.png" alt="Anderson Tuftex" class="product-logo" />
<?php } elseif (get_field('brand') == 'Karastan'){ ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/karastan_logo.png" alt="Karastan" class="product-logo" />
<?php }elseif (get_field('brand') == 'Armstrong') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/armstrong_logo.png" alt="Armstrong" class="product-logo" />
<?php } elseif (get_field('brand') == 'Dream Weaver') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/dreamweaver_logo.png" alt="Dream Weaver" class="product-logo" />
<?php }elseif (get_field('brand') == 'Philadelphia Commercial') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/philadelphia_commercial_logo.png" alt="Philadelphia Commercial" class="product-logo" />
<?php }elseif (get_field('brand') == 'COREtec') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/coretec_logo.png" alt="COREtec" class="product-logo" />
<?php }  elseif (get_field('brand') == 'Daltile') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/daltile_logo.png" alt="Daltile" class="product-logo" />
<?php } elseif (get_field('brand') == 'Floorscapes') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/floorscapes.jpg" alt="Floorscapes" class="product-logo" />
<?php }  elseif (get_field('brand') == 'Mohawk') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/mohawk_logo.png" alt="Mohawk" class="product-logo" />
<?php } elseif (get_field('brand') == 'Bruce') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/bruce_logo.png" alt="Bruce" class="product-logo" />
<?php } elseif (get_field('brand') == 'Mannington') { ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/brand_logo/mannington_logo.png" alt="Mannington" class="product-logo" />
<?php }  else { ?>
    <?php the_field('brand'); ?>
<?php } ?>